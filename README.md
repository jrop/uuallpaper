# uuallpaper

A wallpaper switcher for GNOME powered by Unsplash.

![logo](./logo/cover.png)

<a href="https://gitlab.com/jrop/uuallpaper/-/commits/master">
  <img src="https://gitlab.com/jrop/uuallpaper/badges/master/pipeline.svg" />
</a>


## How to Use

Create a config file:

```toml
# ~/.config/uuallpaper/config.toml
collection = "{YOUR UNSPLASH COLLECTION ID}"
sleep = "15m" # Optional: defaults to 15 minutes
```

Run the switcher:

```sh
$ uuallpaper
```

## Installation

Prerequisites: `go`

```sh
$ sudo GOBIN=/usr/local/bin go install gitlab.com/jrop/uuallpaper@latest
```

To auto-start the daemon, there is a sample `uuallpaper.service` systemd file
that you can place in your user directory (`~/.config/systemd/user/uuallpaper.service`).
Once there, you can enable/run it:

```sh
$ systemctl --user enable uuallpaper.service
$ systemctl --user start uuallpaper.service
```

## Licence (MIT)

MIT License

Copyright (c) 2020 Jonathan Apodaca <jrapodaca@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
