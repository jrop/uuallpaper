//go:build linux
// +build linux

package main

import (
	"embed"
	"errors"
	"fmt"
	"io"
	"os"
	"os/exec"
	"strconv"
	"strings"
	"sync"
)

//go:embed uuallpaper.service
var uuallpaperService embed.FS

func getScreenDimensions() (int, int) {
	cmd := exec.Command("xrandr")
	output, err := cmd.Output()
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error getting screen dimensions: %v\n", err)
		return 0, 0
	}

	screenInfo := strings.Split(string(output), "\n")
	for _, line := range screenInfo {
		if strings.Contains(line, "current") {
			fields := strings.Fields(line)
			width, _ := strconv.Atoi(fields[7])
			height, _ := strconv.Atoi(fields[9][:len(fields[9])-1])
			return width, height
		}
	}

	return 0, 0
}

func setWallpaper(path string) error {
	fehPath, fehErr := exec.LookPath("feh")
	gsettingPath, gsettingErr := exec.LookPath("gsettings")
	if fehErr != nil && gsettingErr != nil {
		return fmt.Errorf("Could not find `feh` or `gsettings`")
	}

	wg := sync.WaitGroup{}

	// feh
	if fehErr == nil {
		wg.Add(1)
		go func() {
			cmd := exec.Command(fehPath, "--bg-scale", path)
			cmd.Stdout = os.Stdout
			cmd.Stderr = os.Stderr
			err := cmd.Run()
			if err != nil {
				fmt.Fprintf(os.Stderr, "Could not invoke `feh`: %v\n", err)
			}
			wg.Done()
		}()
	}

	// gsettings
	if gsettingErr == nil {
		wg.Add(1)
		go func() {
			cmd := exec.Command(
				gsettingPath,
				"set",
				"org.gnome.desktop.background",
				"picture-uri",
				"file://"+path,
			)
			cmd.Stdout = os.Stdout
			cmd.Stderr = os.Stderr
			err := cmd.Run()
			if err != nil {
				fmt.Fprintf(os.Stderr, "Could not invoke `gsettings`i: %v\n", err)
			}
			wg.Done()
		}()
	}

	wg.Wait()
	return nil
}

func installService() error {
	svc, err := uuallpaperService.Open("uuallpaper.service")
	if err != nil {
		return err
	}

	home, err := os.UserHomeDir()
	if err != nil {
		return err
	}

	destFilePath := home + "/.config/systemd/user/uuallpaper.service"
	if _, err := os.Stat(destFilePath); errors.Is(err, os.ErrNotExist) {
		// Create the file (it doesn't exist)
		destFile, err := os.OpenFile(destFilePath, os.O_CREATE|os.O_WRONLY, 0644)
		if err != nil {
			return err
		}

		_, err = io.Copy(destFile, svc)
		if err != nil {
			return err
		}
	}

	cmd := exec.Command("systemctl", "--user", "enable", "--now", "uuallpaper.service")
	err = cmd.Run()
	if err != nil {
		return err
	}

	return nil
}

func uninstallService() error {
	cmd := exec.Command("systemctl", "--user", "disable", "--now", "uuallpaper.service")
	err := cmd.Run()
	if err != nil {
		return err
	}

	home, err := os.UserHomeDir()
	if err != nil {
		return err
	}

	err = os.Remove(home + "/.config/systemd/user/uuallpaper.service")
	if err != nil {
		return err
	}

	return nil
}
