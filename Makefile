all: build/uuallpaper


build/uuallpaper: *.go
	go build -o build/uuallpaper ./...

fmt:
	go run golang.org/x/tools/cmd/goimports -w $(shell find . -name '*.go')
	go run github.com/segmentio/golines -w $(shell find . -name '*.go')
	go fmt ./...

x:
	GOOS=linux GOARCH=amd64 go build -o build/uuallpaper-linux-amd64 ./...
	GOOS=windows GOARCH=amd64 go build -o build/uuallpaper-windows-amd64.exe ./...
	GOOS=darwin GOARCH=amd64 go build -o build/uuallpaper-darwin-amd64 ./...

install: build/uuallpaper
	cp build/uuallpaper /usr/bin/uuallpaper
