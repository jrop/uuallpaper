//go:build windows
// +build windows

// The following is UNTESTED. It was AI generated, but I don't have a Windows
// machine to test it on.

package main

import (
	"fmt"
	"syscall"
	"unsafe"
)

var (
	user32        = syscall.NewLazyDLL("user32.dll")
	system32      = syscall.NewLazyDLL("System32.dll")
	getDC         = user32.NewProc("GetDC")
	getDeviceCaps = user32.NewProc("GetDeviceCaps")
	_setWallpaper = system32.NewProc("SystemParametersInfoW")
)

const (
	HORZRES = 8
	VERTRES = 10

	SPI_SETDESKWALLPAPER = 0x0014
	SPIF_UPDATEINIFILE   = 0x01
	SPIF_SENDCHANGE      = 0x02
)

func getScreenDimensions() (int, int) {
	dc, _, _ := getDC.Call(0)
	width, _, _ := getDeviceCaps.Call(dc, HORZRES)
	height, _, _ := getDeviceCaps.Call(dc, VERTRES)
	return int(width), int(height)
}

func setWallpaper(path string) error {
	wpath, err := syscall.UTF16PtrFromString(path)
	if err != nil {
		return fmt.Errorf("failed to convert path to UTF-16: %v", err)
	}

	r1, _, err := _setWallpaper.Call(
		SPI_SETDESKWALLPAPER,
		0,
		uintptr(unsafe.Pointer(wpath)),
		SPIF_UPDATEINIFILE|SPIF_SENDCHANGE,
	)
	if r1 == 0 {
		return fmt.Errorf("set desktop background: %v", err)
	}

	return nil
}

func installService() error {
	return fmt.Errorf("not implemented")
}

func uninstallService() error {
	return fmt.Errorf("not implemented")
}
