package main

import (
	"fmt"
	"io"
	"net/http"
	"os"
	"path/filepath"
	"time"

	"github.com/BurntSushi/toml"
	"github.com/alecthomas/kong"
)

// CLI struct for the kong package
var cli struct {
	Install   struct{} `cmd:"" help:"install user service"`
	Uninstall struct{} `cmd:"" help:"uninstall user service"`
	Run       struct{} `cmd:"" default:"1"`
}

// Config struct for the TOML config file
type config struct {
	Collection string `toml:"collection"`
	Sleep      string `toml:"sleep"`
}

// Runtime context for the main loop
type runtimeContext struct {
	W             int
	H             int
	CacheDir      string
	Collection    string
	SleepDuration time.Duration
}

func main() {
	ctx := kong.Parse(&cli)
	switch ctx.Command() {
	case "install":
		err := installService()
		if err != nil {
			panic(err)
		}
		break
	case "uninstall":
		err := uninstallService()
		if err != nil {
			panic(err)
		}
		break
	default:
		runForever()
	}
}

// The main loop that is responsible for iterating forever and calling the necessary functions
// to download and set the wallpaper
func runForever() {
	// loop forever
	for {
		ctx, err := runOneIteration()
		if err != nil {
			// if an error occurred, wait 10 seconds, and try again
			fmt.Fprintf(os.Stderr, "error: %v\n", err)
			time.Sleep(time.Second * 10)
			continue
		}

		// otherwise, wait for the configured sleep duration
		time.Sleep(ctx.SleepDuration)
	}
}

// used for cleanup:
var previouslyDownloadedFilePath *string

// The function that composes the necessary operations to set the wallpaper,
// and is run once per iteration of the main loop
func runOneIteration() (*runtimeContext, error) {
	// Read a fresh context in case anything has changed
	ctx, err := makeRuntimeContext()
	if err != nil {
		time.Sleep(time.Second * 5)
		return nil, fmt.Errorf("making runtime context: %v", err)
	}

	// Download a random image:
	downloadedFilePath, err := downloadImg(ctx)
	if err != nil {
		return nil, fmt.Errorf("downloading image: %v", err)
	}

	// Remove previously downloaded file
	if previouslyDownloadedFilePath != nil {
		if err := os.Remove(*previouslyDownloadedFilePath); err != nil {
			return nil, fmt.Errorf("removing previous downloaded file: %v\n", err)
		}
	}
	previouslyDownloadedFilePath = &downloadedFilePath

	// Set wallpaper
	if err := setWallpaper(downloadedFilePath); err != nil {
		return nil, fmt.Errorf("setting wallpaper: %v", err)
	}

	return ctx, nil
}

// Read initial state and the config file
func makeRuntimeContext() (*runtimeContext, error) {
	w, h := getScreenDimensions()

	cacheDir, err := os.UserCacheDir()
	if err != nil {
		panic(err)
	}
	configDir, err := os.UserConfigDir()
	if err != nil {
		panic(err)
	}
	userTomlPath := filepath.Join(configDir, "uuallpaper", "config.toml")
	userToml, err := os.ReadFile(userTomlPath)
	if err != nil {
		return nil, fmt.Errorf("opening user TOML config: %v\n", err)
	}

	var cfg config
	if _, err := toml.Decode(string(userToml), &cfg); err != nil {
		return nil, fmt.Errorf("parsing TOML config: %v\n", err)
	}

	sleepDuration, err := time.ParseDuration(cfg.Sleep)
	if err != nil {
		sleepDuration = 15 * time.Minute
	}

	ctx := runtimeContext{
		W:             w,
		H:             h,
		CacheDir:      cacheDir,
		Collection:    cfg.Collection,
		SleepDuration: sleepDuration,
	}

	return &ctx, nil
}

// Download a random image from unsplash and return the path
func downloadImg(ctx *runtimeContext) (string, error) {
	url := fmt.Sprintf(
		"https://source.unsplash.com/collection/%s/%dx%d",
		ctx.Collection,
		ctx.W,
		ctx.H,
	)
	client := &http.Client{
		Timeout: 10 * time.Second,
	}
	resp, err := client.Get(url)
	if err != nil {
		return "", fmt.Errorf("downloading image: %v", err)
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return "", fmt.Errorf("downloading image: %s", resp.Status)
	}

	destFileName := fmt.Sprintf(
		"%s/uuallpaper_%s.jpg",
		ctx.CacheDir,
		resp.Header.Get("X-Imgix-Id"),
	)
	destFile, err := os.Create(destFileName)
	if err != nil {
		return "", fmt.Errorf("create file: %v", err)
	}
	defer destFile.Close()

	_, err = io.Copy(destFile, resp.Body)
	if err != nil {
		return "", fmt.Errorf("copying download buffer to %s: %v", destFileName, err)
	}

	return destFileName, nil
}
