//go:build darwin
// +build darwin

package main

import (
	"fmt"
	"os"
	"os/exec"
	"strconv"
	"strings"
)

func getScreenDimensions() (int, int) {
	cmd := exec.Command("system_profiler", "SPDisplaysDataType")
	output, err := cmd.Output()
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error getting screen dimensions: %v\n", err)
		return 0, 0
	}

	screenInfo := strings.Split(string(output), "\n")
	for _, line := range screenInfo {
		if strings.Contains(line, "Resolution") {
			fields := strings.Fields(line)
			width, _ := strconv.Atoi(fields[1])
			height, _ := strconv.Atoi(fields[3])
			return width, height
		}
	}

	return 0, 0
}

func setWallpaper(path string) error {
	cmd := exec.Command(
		"osascript",
		"-e",
		fmt.Sprintf(`tell application "Finder" to set desktop picture to POSIX file "%s"`, path),
	)
	err := cmd.Run()
	if err != nil {
		return fmt.Errorf("set desktop background: %v", err)
	}
	return nil
}

func installService() error {
	return fmt.Errorf("not implemented")
}

func uninstallService() error {
	return fmt.Errorf("not implemented")
}
